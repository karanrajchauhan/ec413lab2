.text

main:
	la $s0, my_array		# load base address of my_array into $s0
	lw $s1, array_size		# load size of array
	lw $s2, num_to_find		# load the number to find in array

	move $a0, $s0			# first argument is base address
	move $a1, $s1 			# second argument is array size
	move $a2, $s2			# third argument is the number to find

	jal search 				# call binary search
	
	move $a0, $v0			# print the result of binary search
	li $v0, 1
	syscall

# test search(15)

	li $t0, 15
	sw $t0, num_to_find
	
	move $a0, $s0			# first argument is base address
	move $a1, $s1 			# second argument is array size
	move $a2, $t0			# third argument is the number to find

	jal search 				# call binary search

	move $s3, $v0			# save result in s3

# test search(4)

	li $t0, 4
	sw $t0, num_to_find

	move $a0, $s0			# first argument is base address
	move $a1, $s1 			# second argument is array size
	move $a2, $t0			# third argument is the number to find

	jal search 				# call binary search
	
	move $s4, $v0			# save result in s4

# test search(30)

	li $t0, 30
	sw $t0, num_to_find
	
	move $a0, $s0			# first argument is base address
	move $a1, $s1 			# second argument is array size
	move $a2, $t0			# third argument is the number to find
	
	jal search 				# call binary search
	
	move $s5, $v0			# save result in s5

# test search(0)

	sw $0, num_to_find
	
	move $a0, $s0			# first argument is base address
	move $a1, $s1 			# second argument is array size
	move $a2, $0			# third argument is the number to find
	
	jal search 				# call binary search
	
	move $s6, $v0			# save result in s6

exit:
	li $v0, 10				# stop execution
	syscall

search:
	blez $a1, ret_null		# check if a1 is 0. if so, return -1

	sra $t0, $a1, 1 		# mid index = size/2
	sll $t0, $t0, 2 		# mid index *= 4

	add $t1, $a0, $t0 		# t1 = base addr + mid (t1 is address of mid)
	lw $t3, 0($t1) 			# t3 = array[mid index]

	beq $t3, $a2, ret_mid 	# if t3 == num_to_find, then return
	and $t2, $t2, $0		# initialize t2 to 0
	slt $t2, $t3, $a2 		# t2 is 1 if t3 < num_to_find
	beq $t2, $0, lower 		# t2 is still 0, so t1 > num, so bin search lower

	upper:

	addi $t1, $t1, 4 		# set mid+1 as base of array
	move $a0, $t1

	sra $t0, $t0, 2 		# mid index /= 4 and set that value as new size of array
	move $a1, $t0
	
	j search 				# binary search on upper half

	lower:

	sra $t0, $t0, 2 		# mid index /= 4 and set that value as new size of array
	move $a1, $t0

	j search 				# binary search on lower half

	ret_mid:

	add $a0, $a0, $t0 		# currentbase + mid
	sub $a0, $a0, $s0 		# currentbase + mid - actualbase
	sra $v0, $a0, 2 		# index will be a0 value divide by 4

	jr $ra 					# go to return address

	ret_null:
	
	li $v0, -1
	jr $ra


.data

	my_array:		.word 1 4 5 7 9 12 15 17 18 20 21 30
	array_size:		.word 11
	num_to_find:	.word 9
