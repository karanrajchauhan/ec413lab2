.text

main:

	#Load the "F_" into a0, and print it on the console
	la $a0,result   #Print F_
	li $v0,4
	syscall

	#Load the input num into a0, and print it on the console
	lw $a0,var1
	li $v0,1
	syscall

	#Load "=" into a0, and print it on the cosole
	la $a0,equals
	li $v0,4
	syscall

	lw $t3, ans

	#Load answer into a0, and print it on the cosole
	move $a0,$t3
	li $v0,1
	syscall

exit:
	li $v0, 10				# terminate executaion
	syscall

.data
equals: .asciiz "="
		.space 2
endl:	.asciiz "\n"
		.space 2
var1:	.word 14
ans:	.word 42
result:	.asciiz "F_"
		.space 1